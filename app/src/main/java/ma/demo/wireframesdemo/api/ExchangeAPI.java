package ma.demo.wireframesdemo.api;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Elmehdi Mellouk on 9/22/20.
 * mellouk.elmehdi@gmail.com
 */
public interface ExchangeAPI {


    @GET("/latest")
    Call<JsonElement> getLastByCurrency(@Query("base") String currencyName);

    @GET("/history")
    Call<JsonElement> getHistory(@Query("start_at") String startDate,
                                 @Query("end_at") String endDate,
                                 @Query("symbols") String currencies);


}
