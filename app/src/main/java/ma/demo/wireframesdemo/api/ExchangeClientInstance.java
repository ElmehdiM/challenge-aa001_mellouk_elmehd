package ma.demo.wireframesdemo.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Elmehdi Mellouk on 9/22/20.
 * mellouk.elmehdi@gmail.com
 */
public class ExchangeClientInstance {

    private static Retrofit retrofit;
    private static final String BASE_URL = "https://api.exchangeratesapi.io";

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
