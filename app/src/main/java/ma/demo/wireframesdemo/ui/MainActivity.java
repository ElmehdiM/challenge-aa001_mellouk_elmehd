package ma.demo.wireframesdemo.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import ma.demo.wireframesdemo.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationUI.setupActionBarWithNavController(this, Navigation.findNavController(this,R.id.navHost));


    }



    @Override
    public boolean onSupportNavigateUp() {
        return  Navigation.findNavController(this,R.id.navHost).navigateUp()
                || super.onSupportNavigateUp();
    }
}