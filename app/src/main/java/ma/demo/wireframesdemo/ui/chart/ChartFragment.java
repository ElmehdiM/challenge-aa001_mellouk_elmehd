package ma.demo.wireframesdemo.ui.chart;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Line;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.Anchor;
import com.anychart.enums.MarkerType;
import com.anychart.enums.TooltipPositionMode;
import com.anychart.graphics.vector.Stroke;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import ma.demo.wireframesdemo.CurrencyApp;
import ma.demo.wireframesdemo.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChartFragment extends Fragment {

    private ChartViewModel mViewModel;

    private TextView textCurrencies;
    private AnyChartView anyChartView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.chart_fragment, container, false);

        anyChartView = v.findViewById(R.id.any_chart_view);
        textCurrencies = v.findViewById(R.id.textViewCurrencies);


        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ChartViewModel.class);

        mViewModel.selectedCurrency = getArguments().getString("selectedCurrency");
        mViewModel.historyCurrency = getArguments().getString("historyCurrency");

        textCurrencies.setText(getString(R.string.text_currencies,mViewModel.selectedCurrency,mViewModel.historyCurrency));

        pullHistory();

    }


    private void pullHistory() {
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.MONTH, -2);

        Date dateStart = cal.getTime();

        String startDate = format.format(dateStart);
        String endDate = format.format(new Date());
        String symbols = mViewModel.selectedCurrency + "," + mViewModel.historyCurrency;

        Log.d("tag", "startDate >> " + startDate);
        Log.d("tag", "endDate >> " + endDate);

        Call<JsonElement> call = CurrencyApp.api().getHistory(startDate, endDate, symbols);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                if(response.body() != null) {
                    Log.d("tag", "response == " + response.body());

                    try {
                        JSONObject jsonObject = new JSONObject((response.body().getAsJsonObject()).getAsJsonObject("rates").toString());

                        Iterator<String> names = jsonObject.keys();

                        String date = "";
                        Float selectedValue = 0f;
                        Float historyValue = 0f;

                        while (names.hasNext()) {
                            date = names.next();

                            JSONObject rates = jsonObject.optJSONObject(date);


                            selectedValue = Float.parseFloat(rates.optString(mViewModel.selectedCurrency));
                            historyValue = Float.parseFloat(rates.optString(mViewModel.historyCurrency));


                            mViewModel.seriesData.add(new CustomDataEntry(date, selectedValue, historyValue));

                        }


                        showChartView();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                    Snackbar.make(getView(),getString(R.string.error_json), Snackbar.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.d("tag", "error == " + t.fillInStackTrace());

                Snackbar.make(getView(),getString(R.string.error_json), Snackbar.LENGTH_SHORT).show();


            }
        });
    }

    private void showChartView() {

        Cartesian cartesian = AnyChart.line();
        cartesian.animation(true);

        cartesian.padding(10d, 20d, 5d, 20d);

        cartesian.crosshair().enabled(true);
        cartesian.crosshair()
                .yLabel(true)
                .yStroke((Stroke) null, null, null, (String) null, (String) null);

        cartesian.tooltip().positionMode(TooltipPositionMode.POINT);

        cartesian.title(getString(R.string.desc_rates));

        cartesian.xAxis(0).labels().padding(5d, 5d, 5d, 5d);

        Set set = Set.instantiate();
        set.data(mViewModel.seriesData);
        Mapping series1Mapping = set.mapAs("{ x: 'x', value: 'value' }");
        Mapping series2Mapping = set.mapAs("{ x: 'x', value: 'value2' }");

        Line series1 = cartesian.line(series1Mapping);
        series1.name(mViewModel.selectedCurrency);
        series1.hovered().markers().enabled(true);
        series1.hovered().markers()
                .type(MarkerType.CIRCLE)
                .size(4d);
        series1.tooltip()
                .position("right")
                .anchor(Anchor.LEFT_CENTER)
                .offsetX(5d)
                .offsetY(5d);

        Line series2 = cartesian.line(series2Mapping);
        series2.name(mViewModel.historyCurrency);
        series2.hovered().markers().enabled(true);
        series2.hovered().markers()
                .type(MarkerType.CIRCLE)
                .size(4d);
        series2.tooltip()
                .position("right")
                .anchor(Anchor.LEFT_CENTER)
                .offsetX(5d)
                .offsetY(5d);


        cartesian.legend().enabled(true);
        cartesian.legend().fontSize(13d);
        cartesian.legend().padding(0d, 0d, 10d, 0d);

        anyChartView.setChart(cartesian);
    }

    private class CustomDataEntry extends ValueDataEntry {

        CustomDataEntry(String x, Number value, Number value2) {
            super(x, value);
            setValue("value2", value2);
        }
    }

}