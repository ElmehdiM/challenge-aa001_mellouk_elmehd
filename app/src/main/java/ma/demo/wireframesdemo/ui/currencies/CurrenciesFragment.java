package ma.demo.wireframesdemo.ui.currencies;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import ma.demo.wireframesdemo.CurrencyApp;
import ma.demo.wireframesdemo.R;
import ma.demo.wireframesdemo.model.Currency;
import ma.demo.wireframesdemo.ui.currencies.adapter.CurrenciesRecyclerAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrenciesFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private CurrenciesViewModel mViewModel;

    private RecyclerView recyclerView;
    private Spinner currencySpinner;
    private SwipeRefreshLayout refreshLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.wire_frame_fragment, container, false);

        recyclerView = v.findViewById(R.id.recyclerCurrency);
        currencySpinner = v.findViewById(R.id.spinnerCurrency);

        refreshLayout = v.findViewById(R.id.refreshLayout);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CurrenciesViewModel.class);


        pullCurrencies(mViewModel.selectedCurrency);
        refreshLayout.setOnRefreshListener(() -> {
            pullCurrencies(mViewModel.selectedCurrency);
        });


        currencySpinner.setOnItemSelectedListener(this);

    }

    private void pullCurrencies(String currencyName) {
        showProgress();

        Call<JsonElement> call = CurrencyApp.api().getLastByCurrency(currencyName);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                try {
                    JsonObject jsonObject = response.body().getAsJsonObject();

                    mViewModel.currencies = Currency.converter(new JSONObject(jsonObject.get("rates").toString()));

                    showCurrencies();

                    populateSpinner();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.d("tag", "error == " + t.fillInStackTrace());

            }
        });
    }

    private void populateSpinner() {

        if (currencySpinner.getSelectedItem() == null) {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mViewModel.currenciesName());
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            currencySpinner.setAdapter(adapter);

            mViewModel.selectedCurrency = mViewModel.currenciesName()[0];
        }

    }

    private void showCurrencies() {

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new CurrenciesRecyclerAdapter(mViewModel.currencies, currency -> {
            Log.d("tag", "currency == " + currency.getCurrencyValue());

            Bundle bundle = new Bundle();
            bundle.putString("selectedCurrency",mViewModel.selectedCurrency);
            bundle.putString("historyCurrency",currency.getCurrencyName());

            Navigation.findNavController(getView()).navigate(R.id.action_wireFrameFragment_to_chartFragment,bundle);

        }));

        hideProgress();

    }


    private void showProgress() {
        refreshLayout.setRefreshing(true);
    }

    private void hideProgress() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        mViewModel.selectedCurrency = mViewModel.currenciesName()[i];
        pullCurrencies(mViewModel.selectedCurrency);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}