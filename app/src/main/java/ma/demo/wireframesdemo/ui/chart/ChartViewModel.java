package ma.demo.wireframesdemo.ui.chart;

import androidx.lifecycle.ViewModel;

import com.anychart.chart.common.dataentry.DataEntry;

import java.util.ArrayList;
import java.util.List;

public class ChartViewModel extends ViewModel {
    List<DataEntry> seriesData = new ArrayList<>();

    public String selectedCurrency;
    public String historyCurrency;
}