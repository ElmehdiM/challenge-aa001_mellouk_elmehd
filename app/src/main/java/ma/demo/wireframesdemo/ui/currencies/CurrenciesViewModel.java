package ma.demo.wireframesdemo.ui.currencies;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import ma.demo.wireframesdemo.model.Currency;

public class CurrenciesViewModel extends ViewModel {

    public List<Currency> currencies = new ArrayList<>();
    public String selectedCurrency = "USD";

    public String[] currenciesName(){
        String[] names = new String[currencies.size()];
        for (int i = 0; i < currencies.size(); i++) {
            names[i] = currencies.get(i).getCurrencyName();
        }

        return names;
    }
}