package ma.demo.wireframesdemo.ui.currencies.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ma.demo.wireframesdemo.R;
import ma.demo.wireframesdemo.model.Currency;


/**
 * Created by Elmehdi Mellouk on 9/22/20.
 * mellouk.elmehdi@gmail.com
 */
public class CurrenciesRecyclerAdapter extends RecyclerView.Adapter<CurrenciesRecyclerAdapter.CurrenciesViewHolder> {
    private final OnClickListener mOnClickListener;
    private List<Currency> items;

    public CurrenciesRecyclerAdapter(List<Currency> items, OnClickListener onClickListener) {
        this.items = items;
        this.mOnClickListener = onClickListener;
    }

    @Override
    public CurrenciesViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.currency_custom, parent, false);

        return new CurrenciesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CurrenciesViewHolder holder, int position) {
        Currency item = items.get(position);
        holder.set(item);
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class CurrenciesViewHolder extends RecyclerView.ViewHolder {

        public CurrenciesViewHolder(View itemView) {
            super(itemView);
        }

        public void set(Currency item) {
            ((TextView)itemView.findViewById(R.id.textCurrency)).setText(item.getCurrencyName());
            ((TextView)itemView.findViewById(R.id.textValue)).setText(item.getCurrencyValue());
            (itemView.findViewById(R.id.imageChart)).setOnClickListener(view -> {
                mOnClickListener.onClick(item);
            });
        }
    }

    public interface OnClickListener{
        void onClick(Currency currency);
    }
}

