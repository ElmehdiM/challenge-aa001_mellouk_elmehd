package ma.demo.wireframesdemo.model;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Elmehdi Mellouk on 9/22/20.
 * mellouk.elmehdi@gmail.com
 */
public class Currency {
    private String currencyName;
    private String currencyValue;

    public Currency(){}

    public Currency(String currencyName, String currencyValue) {
        this.currencyName = currencyName;
        this.currencyValue = currencyValue;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencyValue() {
        return currencyValue;
    }

    public void setCurrencyValue(String currencyValue) {
        this.currencyValue = currencyValue;
    }

    public static List<Currency> converter(JSONObject rates){
        List<Currency> currencies = new ArrayList<>();
            Iterator<String> names = rates.keys();
            while (names.hasNext()){
                String name = names.next();
                String value = rates.optString(name);
                currencies.add(new Currency(name,value));
            }


        return currencies;
    }
}
