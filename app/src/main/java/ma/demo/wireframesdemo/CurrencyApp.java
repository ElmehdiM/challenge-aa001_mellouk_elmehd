package ma.demo.wireframesdemo;

import android.app.Application;

import ma.demo.wireframesdemo.api.ExchangeAPI;
import ma.demo.wireframesdemo.api.ExchangeClientInstance;

/**
 * Created by Elmehdi Mellouk on 9/22/20.
 * mellouk.elmehdi@gmail.com
 */
public class CurrencyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static ExchangeAPI api(){
        return ExchangeClientInstance.getRetrofitInstance().create(ExchangeAPI.class);
    }
}
